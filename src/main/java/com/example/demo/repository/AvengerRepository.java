package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Avenger;

@Repository
public interface AvengerRepository extends CrudRepository<Avenger, Long>, JpaSpecificationExecutor<Avenger>{

}

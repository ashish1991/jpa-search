package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.SearchReqDto;
import com.example.demo.service.HeroService;

@RestController
@RequestMapping("/hero")
public class HeroController {

	private final HeroService heroService;
	
	@Autowired
	public HeroController(HeroService heroService) {
		this.heroService = heroService;
	}

	@PostMapping("searchMarvel")
	public ResponseEntity<?> searchMarvel(@RequestBody SearchReqDto reqDto){
		return ResponseEntity.ok(heroService.searchMarvel(reqDto));
	}
	
	@PostMapping("searchDc")
	public ResponseEntity<?> searchDc(@RequestBody SearchReqDto reqDto){
		return ResponseEntity.ok(heroService.searchDc(reqDto));
	}
}

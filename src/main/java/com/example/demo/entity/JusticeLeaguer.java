package com.example.demo.entity;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class JusticeLeaguer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstName;
	private String lastName;
	private String alias;
	private int age;
	private BigDecimal powerRating;
	private boolean seniorMembers;
	private Instant joiningDate = Instant.now();

	public JusticeLeaguer(String firstName, String lastName, String alias, int age, BigDecimal powerRating,
			boolean seniorMembers) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.alias = alias;
		this.age = age;
		this.powerRating = powerRating;
		this.seniorMembers = seniorMembers;
	}
}

package com.example.demo.dto;

import java.math.BigDecimal;
import java.time.Instant;

import lombok.Data;

@Data
public class HeroDto {
	private String firstName;
	private String lastName;
	private String alias;
	private int age;
	private boolean seniorMembers;
	private BigDecimal powerRating;
	private Instant joiningDate;
}

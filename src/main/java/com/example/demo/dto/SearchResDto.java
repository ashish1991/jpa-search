package com.example.demo.dto;

import java.util.List;

import lombok.Data;

@Data
public class SearchResDto {
	private List<Object> heroes;
	private int totalPages;
	private int pageIndex;
	private long totalRecords;
}

package com.example.demo.service.impl;

import static com.example.demo.util.Constants.DEFAULT_PROP;
import static com.example.demo.util.SearchUtil.createCopyObject;
import static com.example.demo.util.SearchUtil.createSpec;
import static com.example.demo.util.SearchUtil.getOrders;
import static com.example.demo.util.SearchUtil.prepareResponseForSearch;
import static java.util.Arrays.asList;
import static org.springframework.data.domain.Sort.by;

import java.math.BigDecimal;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.demo.dto.HeroDto;
import com.example.demo.dto.SearchReqDto;
import com.example.demo.dto.SearchResDto;
import com.example.demo.entity.Avenger;
import com.example.demo.entity.JusticeLeaguer;
import com.example.demo.repository.AvengerRepository;
import com.example.demo.repository.JusticeLeaguerRepository;
import com.example.demo.service.HeroService;

@Service
public class HeroServiceImpl implements HeroService {

	private final AvengerRepository avengerRepository;
	private final JusticeLeaguerRepository justiceLeaguerRepository;

	@Autowired
	public HeroServiceImpl(AvengerRepository avengerRepository, JusticeLeaguerRepository JusticeLeaguerRepository) {
		this.avengerRepository = avengerRepository;
		this.justiceLeaguerRepository = JusticeLeaguerRepository;
	}

	@Override
	public SearchResDto searchMarvel(SearchReqDto reqDto) {
		PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
				by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
		Page<Avenger> page = avengerRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);
		Function<Avenger, HeroDto> mapper = (hero) -> createCopyObject(hero, HeroDto::new);
		return prepareResponseForSearch(page, mapper);
	}

	@Override
	public SearchResDto searchDc(SearchReqDto reqDto) {
		PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
				by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
		Page<JusticeLeaguer> page = justiceLeaguerRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);
		Function<JusticeLeaguer, HeroDto> mapper = (hero) -> createCopyObject(hero, HeroDto::new);
		return prepareResponseForSearch(page, mapper);
	}

	// create test data
	@PostConstruct
	public void createTestData() {
		avengerRepository.saveAll(asList(new Avenger("Tony", "Stark", "Iron Man", 40, new BigDecimal(9.8f), true),
				new Avenger("Bruce", "Banners", "Hulk", 35, new BigDecimal(8.2f), true),
				new Avenger("Jennifer", "Walters", "She Hulk", 30, new BigDecimal(8.2f), true),
				new Avenger("Steve", "Rogers", "Captain America", 32, new BigDecimal(9.9f), true),
				new Avenger("Clint", "Barton", "Hawkeye", 36, new BigDecimal(7.5f), true),
				new Avenger("Sam", "Wilson", "Falcon", 29, new BigDecimal(6.6f), false),
				new Avenger("Peter", "Parker", "Spiderman", 20, new BigDecimal(9), false),
				new Avenger("Nick", "Fury", "Fury", 45, new BigDecimal(8.1f), true),
				new Avenger("Scott", "Lang", "Antman", 37, new BigDecimal(8.3f), false),
				new Avenger("Nathasha", "Romannoff", "Black Widow", 33, new BigDecimal(9.2f), true),
				new Avenger("Thor", "Odinson", "Thor", 1025, new BigDecimal(9.9f), true),
				new Avenger("Wanda", "Maximoff", "Scarlett Witch", 26, new BigDecimal(9), false)));
		justiceLeaguerRepository
				.saveAll(asList(new JusticeLeaguer("Bruce", "Wayne", "BATMAN", 39, new BigDecimal(20), true),
						new JusticeLeaguer("Clark", "Kent", "Superman", 32, new BigDecimal(10), true),
						new JusticeLeaguer("Diana", "Prince", "Wonder Woman", 1051, new BigDecimal(10), true),
						new JusticeLeaguer("Barry", "Allen", "Flash", 26, new BigDecimal(9.9f), true),
						new JusticeLeaguer("Oliver", "Queen", "Green Arrow", 30, new BigDecimal(9), false),
						new JusticeLeaguer("Hal", "Jordan", "Green Lantern", 31, new BigDecimal(9.55f), true),
						new JusticeLeaguer("Jonn", "Jonnz", "Martian Manhunter", 745, new BigDecimal(9.2f), true),
						new JusticeLeaguer("Billy", "Batson", "SHAZAM", 15, new BigDecimal(9.5f), false),
						new JusticeLeaguer("Dinah", "Lang", "Black Canary", 28, new BigDecimal(8.5f), false),
						new JusticeLeaguer("Kara", "Danvers", "Supergirl", 22, new BigDecimal(9.1f), false)));
	}
}

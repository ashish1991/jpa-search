package com.example.demo.service;

import com.example.demo.dto.SearchReqDto;
import com.example.demo.dto.SearchResDto;

public interface HeroService {

	SearchResDto searchMarvel(SearchReqDto reqDto);
	SearchResDto searchDc(SearchReqDto reqDto);

}
